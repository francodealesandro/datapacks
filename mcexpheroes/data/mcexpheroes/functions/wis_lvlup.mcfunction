scoreboard players set @s wis_exp 0
scoreboard players add @s wis_lvl 1
scoreboard players add @s[scores={class=4}] wis_lvl 1
title @s title ["",{"text":"LEVEL UP","color":"gold","bold":true}]
title @s subtitle ["",{"text":"Wisdom is now level ","color":"white"},{"score":{"name":"@s","objective":"wis_lvl"},"color":"gold"}]
playsound minecraft:entity.arrow.hit_player master @s ~ ~ ~ 1 0.5
playsound minecraft:entity.arrow.hit_player master @s ~ ~ ~ 1 1
tellraw @s ["",{"text":"Wisdom is now level ","color":"green"},{"score":{"name":"@s","objective":"wis_lvl"},"color":"gold"}]
particle minecraft:firework ~ ~ ~ 0 0 0 1 300 normal @s
tellraw @s[scores={wis_lvl=100..}] ["",{"text":"Skill is maxed!","color":"gold"}]

