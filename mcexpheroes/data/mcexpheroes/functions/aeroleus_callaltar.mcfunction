tellraw @a ["",{"text":"Calling the altar of Aeroleus.","color":"light_purple"}]
tp ~ ~3 ~
execute at @s run fill ~-5 ~-5 ~-5 ~5 ~5 ~5 air
execute at @s run fill ~-1 ~-1 ~-1 ~1 ~-1 ~1 chiseled_stone_bricks
execute at @s run fill ~-2 ~4 ~-2 ~2 ~4 ~2 stone_brick_slab
execute at @s run fill ~-2 ~-2 ~-2 ~2 ~-2 ~2 minecraft:stone_bricks
execute at @s run fill ~-3 ~-3 ~-3 ~3 ~-3 ~3 minecraft:stone_bricks
execute at @s run fill ~1 ~ ~1 ~1 ~3 ~1 chiseled_stone_bricks
execute at @s run fill ~-1 ~ ~1 ~-1 ~3 ~1 chiseled_stone_bricks
execute at @s run fill ~1 ~ ~-1 ~1 ~3 ~-1 chiseled_stone_bricks
execute at @s run fill ~-1 ~ ~-1 ~-1 ~3 ~-1 chiseled_stone_bricks
execute at @s run setblock ~ ~ ~ torch
execute at @s run setblock ~ ~-1 ~ diamond_block
execute at @s run summon armor_stand ~ ~ ~ {NoGravity:1b,Invulnerable:1b,Marker:1b,Invisible:1b,PersistenceRequired:1b,Tags:["aero_altar"]}

