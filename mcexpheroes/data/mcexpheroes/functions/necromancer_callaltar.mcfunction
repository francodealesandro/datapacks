tellraw @a ["",{"text":"Calling the altar of Necromancer.","color":"light_purple"}]
tp ~ ~3 ~
execute at @s run fill ~-4 ~-3 ~-4 ~4 ~3 ~4 air
execute at @s run fill ~-1 ~-1 ~-1 ~1 ~-1 ~1 minecraft:bone_block
execute at @s run fill ~-2 ~-2 ~-2 ~2 ~-2 ~2 minecraft:stone
execute at @s run fill ~-3 ~-3 ~-3 ~3 ~-3 ~3 minecraft:stone
execute at @s run fill ~1 ~ ~1 ~1 ~3 ~1 bone_block
execute at @s run fill ~-1 ~ ~1 ~-1 ~3 ~1 bone_block
execute at @s run fill ~1 ~ ~-1 ~1 ~3 ~-1 bone_block
execute at @s run fill ~-1 ~ ~-1 ~-1 ~3 ~-1 bone_block
execute at @s run setblock ~ ~ ~ skeleton_skull
execute at @s run setblock ~ ~-1 ~ purpur_pillar
execute at @s run summon armor_stand ~ ~ ~ {NoGravity:1b,Invulnerable:1b,Marker:1b,Invisible:1b,PersistenceRequired:1b,Tags:["necro_altar"]}

