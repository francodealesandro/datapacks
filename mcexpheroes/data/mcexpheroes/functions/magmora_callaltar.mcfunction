tellraw @a ["",{"text":"Calling the altar of Magmora.","color":"light_purple"}]
execute at @s run fill ~-5 ~ ~-5 ~5 ~ ~5 air
execute at @s run fill ~-3 ~-1 ~-3 ~3 ~-1 ~3 nether_bricks
execute at @s run fill ~1 ~ ~1 ~1 ~3 ~1 nether_brick_fence
execute at @s run fill ~-1 ~ ~1 ~-1 ~3 ~1 nether_brick_fence
execute at @s run fill ~1 ~ ~-1 ~1 ~3 ~-1 nether_brick_fence
execute at @s run fill ~-1 ~ ~-1 ~-1 ~3 ~-1 nether_brick_fence
execute at @s run setblock ~ ~-1 ~ glowstone
execute at @s run summon armor_stand ~ ~ ~ {NoGravity:1b,Invulnerable:1b,Marker:1b,Invisible:1b,PersistenceRequired:1b,Tags:["magma_altar"]}

