tellraw @a ["",{"text":"Calling the altar of Aquarius.","color":"light_purple"}]
tp ~ ~3 ~
execute at @s if block ~ ~ ~ water run fill ~-5 ~-3 ~-5 ~7 ~5 ~5 water
execute at @s run fill ~-1 ~-1 ~-1 ~1 ~-1 ~1 prismarine_bricks
execute at @s run fill ~-2 ~4 ~-2 ~2 ~4 ~2 prismarine_brick_slab
execute at @s run fill ~-2 ~-2 ~-2 ~2 ~-2 ~2 prismarine
execute at @s run fill ~-3 ~-3 ~-3 ~3 ~-3 ~3 prismarine
execute at @s run fill ~1 ~ ~1 ~1 ~6 ~1 prismarine_bricks
execute at @s run fill ~-1 ~ ~1 ~-1 ~6 ~1 prismarine_bricks
execute at @s run fill ~1 ~ ~-1 ~1 ~6 ~-1 prismarine_bricks
execute at @s run fill ~-1 ~ ~-1 ~-1 ~6 ~-1 prismarine_bricks
execute at @s run setblock ~ ~-1 ~ sea_lantern
execute at @s run setblock ~ ~4 ~ sea_lantern
execute at @s run summon armor_stand ~ ~ ~ {NoGravity:1b,Invulnerable:1b,Marker:1b,Invisible:1b,PersistenceRequired:1b,Tags:["aqua_altar"]}

